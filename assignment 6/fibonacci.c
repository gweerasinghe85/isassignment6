#include <stdio.h>

int fibonacciSeq(int);
int fib(int);

int i=0,num=0;

int fib(int a)
{
	if(a==0)
		return 0;
	else if(a==1)
		return 1;
	else
		return (fib(a-1)+fib(a-2));
}

int fibonacciSeq(int n)
{
	if(i<=n)
	{
		printf("%d \n",fib(num));
		num=num+1;
		i=i+1;
		fibonacciSeq(n);
	}
	else 
		return 0;
}

int main()
{
	int n;
	printf("Enter the number of terms: ");
	scanf("%d",&n);
	fibonacciSeq(n);
}
