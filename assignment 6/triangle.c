#include <stdio.h>

void triangle(int);
void row(int);

int rownum=1;

void triangle(int x)
{
	if(x>0)
	{
		row(rownum);
		printf("\n");
		rownum=rownum+1;
		triangle(x-1);
	}
}

void row(int y)
{
	if(y>0)
	{
		printf("%d",y);
		row(y-1);
	}
}

int main()
{
	int x;
	printf("Enter the number of rows: ");
	scanf("%d",&x);
	triangle(x);
	return 0;
}
